#include <stdio.h>
#include "queue.h"
#include <string.h>
#define SIZE_OF_ARRAY 6



int dequeue(queue* q)
{

	if (!q ||q->count == 0)
	{
		printf("[!] Queue is empty");
		return -1;
	}
	printf("[*] Pop -> %d\n",q->arr[(q->count)-1]);
	q->count--;
	return 0;
}


void enqueue(queue* q, unsigned int newValue)
{
	if (q)
	{
		q->arr[q->count] = newValue;
		q->count++;
	}
	else
	{
		printf("[!] Cant't add queue is empty");
	}
}



void initQueue(queue* q, unsigned int size)
{
	
	q->arr = new int[sizeof(int) * size];
	q->count = 0;
}

void cleanQueue(queue* q)
{
	if (q)
	{
		q->count = 0;
		if (q->arr)
		{
			delete(q->arr);
		}
		delete(q);
	}
}