#include "linkedList.h"
#include <stdio.h>

/*
Function adds element to the node list
Input: pointer to the head pointer in orfer the change by reference
Output: None
*/
void addElement(nodeList** head , int element)
{
	nodeList* newNode = new nodeList[sizeof(nodeList)];
	newNode->nextElement = (*head);
	newNode->data = element;
	(*head) = newNode;
}

/*
Function removes element from the node list
Input: pointer to the head pointer in orfer the change by reference
Output: removed element
*/

int removeElement(nodeList** head)
{
	nodeList* temp = NULL;
	int reti = -1; 
	if ((*head))
	{
		reti = (*head)->data;
		temp = (*head);
		(*head) = (*head)->nextElement;
	}
	//else
	//{
	//	printf("[*] List is empty");
	//}
	return reti;
}


/*
Function initates list
Input: pointer to the nodeList head
Output: None
*/
void initList(nodeList* s)
{
	s->data = 0;
	s->nextElement = NULL;
}
