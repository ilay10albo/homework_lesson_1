#include "linkedList.h"
#include "stack.h"
#include <iostream>
using namespace std;

/*
	Function prints the stack content
	Input: stack pointer
	Output: None
*/
void toString(stack* s)
{
	cout << "[=] Displaying current stack\n";
	int counter = 0;
	while (s->stk)
	{
		std::cout << (counter + 1) << ". -> " << s->stk->data <<std::endl;
		s->stk = s->stk->nextElement;
		counter++;
	}
	cout << "----------------------------\n";
}

/*
Function checks if stack is empty or not
Input: stack
Output: true or false
*/
bool isEmpty(stack *s)
{
	int result = pop(s);
	if (result == -1)
	{
		return true;
	}
	push(s , result);
	return false;
	
}

/*
Function clears the stack
Input: stack 
Output: None
*/
void cleanStack(stack* s)
{
	while (s->stk)
	{
		pop(s);
		s->stk = s->stk;
	}
}

/*
Function initiates stack
Input: stack
Output: None
*/
void initStack(stack* s)
{
	s->stk = new nodeList[sizeof(nodeList)];
	initList(s->stk);
	pop(s);
}

/*
Function pops an element from stack
Input: stack pointer
Output: poped element
*/
int pop(stack* s)
{
	return removeElement(&(s->stk));
}

/*
Function inserts new elementto the stack
Input:
Output:
*/
void push(stack* s, unsigned int element)
{
	addElement(&(s->stk), element);
}