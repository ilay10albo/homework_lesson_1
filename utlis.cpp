#include "utils.h"
#include "stack.h"
using namespace std;
#define MAX_SIZE 10
#define ZERO 0
/*
	Function takes an array and reverses it with the uses
	of stack.
	Input: array , size of array
	Output: None
*/
void reverse(int* nums, unsigned int size)
{
	stack* stc = new stack[sizeof(stack)];
	initStack(stc);
	for (int i = ZERO; i < size; i++)
	{
		push(stc, nums[i]);
	}
	for ( int i = ZERO;  i < size;  i++)
	{
		nums[i] = pop(stc);
		cout << nums[i] << std::endl;
	}
	cleanStack(stc); // Delocate memory
}

/*
	Function reqeusts the user for ten numbers
	Input: None
	Output: Function prints the number array in
	Opposite direction.
*/
int* reverse10()
{
	int numArray[MAX_SIZE] = { ZERO }; // Reset array
	for (int  i = ZERO; i < MAX_SIZE; i++)
	{
		cout << "Enter the " << i+1<< " number: ";
		cin >> numArray[i];
		getchar();
	}
	reverse(numArray, MAX_SIZE);
	return numArray;
}