#pragma once
#include <iostream>

typedef struct nodeList
{
	int data;
	nodeList* nextElement;
} nodeList;

void addElement(nodeList** head, int element);
int removeElement(nodeList** head);
void initList(nodeList* s);